/**
 * @file
 * Provides javascript functionality for image buffer widget.
 */

function htmlToElement (html) {
  let template = document.createElement('template')
  template.innerHTML = html.trim()
  return template.content.firstChild
}

(function ($, Drupal) {

  /**
   * Handler for the upload image from a buffer.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches site-wide featured behaviors.
   */
  Drupal.behaviors.imageBufferWidget = {

    attach (context, settings) {
      if (navigator.permissions) {
        // Attach handler for click on the upload from buffer button.
        $(context).find('input[data-drupal-selector$="upload-buffer-button"]').once('auto-upload-image-buffer').on('click', this.onClickUploadBufferButton)
      } else {
        // If browser doesn't supporting permissions, hide the upload from buffer button.
        $(context).find('input[data-drupal-selector$="upload-buffer-button"]').css('display', 'none')
      }
    },
    detach (context) {
      if (navigator.permissions) {
        $(context).find('input[data-drupal-selector$="upload-buffer-button"]').removeOnce('auto-upload-image-buffer').off('click')
      }
    },
    async onClickUploadBufferButton (event) {
      // Clean previous errors.
      event.currentTarget.closest('.image-widget').querySelectorAll('.file-upload-js-error').forEach(node => node.remove())
      // Add attribute enctype = "multipart/form-data".
      // Because Drupal is not adding type, if the file fields are not existing in form!
      if (!event.currentTarget.form.getAttribute('enctype')) event.currentTarget.form.setAttribute('enctype', 'multipart/form-data')

      navigator.permissions.query({ name: 'clipboard-read' }).then(async result => {
        // If permission to read the clipboard is granted,
        // or if the user will be prompted to allow it, we proceed.
        if (result.state === 'granted' || result.state === 'prompt') {
          try {
            const clipboardItems = await navigator.clipboard.read()
            if (clipboardItems.length)
              try {
                const blobOutput = await clipboardItems[0].getType('image/png')
                // Generate new file from the content in clipboard.
                const name = `image-${new Date().getTime()}.jpg`
                let filesList = [ new File([ await blobOutput.arrayBuffer() ], name, { type: blobOutput.type }) ]
                filesList.__proto__ = Object.create(FileList.prototype)

                // Set files to input.
                let fileInput = event.currentTarget.parentNode.querySelector('input[type="file"]')
                Object.defineProperty(fileInput, 'files', { configurable: true, value: filesList })
                // Trigger the change event for start the uploading.
                fileInput.dispatchEvent(new Event('change'))
                Drupal.file.disableFields(event)
              } finally {}
          } catch (err) {
            // Add standard error.
            const error = Drupal.t('The content of clipboard cannot be uploaded.')
            event.currentTarget.closest('div.js-form-managed-file').prepend(
              htmlToElement('<div class="messages messages--error file-upload-js-error" aria-live="polite">' + error + '</div>')
            )
          }
        } else {
          console.log('Clipboard read is declined!')
        }
      })

      event.preventDefault()
    }

  }

})(jQuery, Drupal)
