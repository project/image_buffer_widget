Image Buffer Widget
===================

Provides an image widget that allows users upload image from a buffer.

How it works?
-------------

Only modern browsers supporting.
Site should use https protocol. User should allow the reading of clipboard.
The widget extends the core's image widget - add one more button "Buffer".
All other logic in js code.
