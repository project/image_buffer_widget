<?php

namespace Drupal\image_buffer_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an image widget that allows uploading image from buffer.
 *
 * @FieldWidget(
 *   id = "image_buffer_widget",
 *   label = @Translation("Image Buffer Widget"),
 *   field_types = {
 *     "image",
 *   },
 * )
 */
class ImageBufferWidget extends ImageWidget {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = parent::create($container, $configuration, $plugin_id,$plugin_definition);
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    // @TODO why it fires before image_image, could use alter to image_image and
    //   core's media same time.
    if (empty($element['#value']['fids'])) {
      // If no file, show the upload from buffer button before image_image.
      $element['upload_buffer_button'] = [
        '#type' => 'button',
        '#value' => new TranslatableMarkup('Buffer'),
        '#attributes' => [
          'title' => new TranslatableMarkup('Upload image from Clipboard'),
        ],
        '#attached' => [
          'library' => [
            'image_buffer_widget/upload_buffer',
          ],
        ],
      ];
    }

    return parent::process($element, $form_state, $form);
  }

}
